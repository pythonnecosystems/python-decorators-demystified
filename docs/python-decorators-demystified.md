# Python 데코레이터 이해하기: 고급 기법으로 코드의 완성도 높이기

## 소개
숙련된 Python 개발자에게 '비밀 무기'로 여겨지는 Python 데코레이터는 코드를 우아하게 개선할 수 있는 역동적이고 강력한 기능이다. 이 포괄적인 가이드에서는 데코레이터의 내부 작동 방식, 고급 사용 사례 및 실제 어플리케이션을 살펴보면서 데코레이터의 복잡한 기능에 대해 자세히 살펴본다. 데코레이터를 처음 사용하든 기술을 연마하든, 이 가이드는 이 필수 Python 기능을 마스터하기 위한 관문이다.

## Python 데코레이터 이해하기
데코레이터는 함수나 메서드를 감싸는 래퍼 역할을 하여 원래 코드를 변경하지 않고도 그 동작을 확장하거나 수정할 수 있게 해준다. 데코레이터는 코드 가독성을 높이고, 코드 재사용을 촉진하며, 프로그래밍에 모듈식 접근 방식을 도입할 수 있도록 한다.

## 데코레이터의 해부학
데코레이터의 구조를 해부하여 그 구성 요소를 파악해 보자.

```python
def my_decorator(func):
    def wrapper(*args, **kwargs):
        print("Executing something before the function is called.")
        result = func(*args, **kwargs)
        print("Executing something after the function is called.")
        return result
    return wrapper

@my_decorator
def say_hello():
    print("Hello!")

say_hello()
```

Output:

```
Executing something before the function is called.
Hello!
Executing something after the function is called.
```

여기서 `my_decorator`는 `say_hello` 함수를 래핑하여 함수 호출 전후에 코드를 실행할 수 있는 기능을 보여준다.

## 사용 사례: 데코레이터가 뛰어난 경우
데코레이터가 빛을 발하는 실용적인 시나리오를 살펴보고 코드를 새로운 차원으로 끌어올리세요.

### Logging 데코레이터

```python
def log_decorator(func):
    def wrapper(*args, **kwargs):
        print(f"Calling {func.__name__} with arguments {args} {kwargs}")
        result = func(*args, **kwargs)
        print(f"{func.__name__} returned {result}")
        return result
    return wrapper

@log_decorator
def add_numbers(a, b):
    return a + b

result = add_numbers(3, 5)
```

Output:

```
Calling add_numbers with arguments (3, 5) {}
add_numbers returned 8
```

### Timing 데코레이터

```python
import time

def timing_decorator(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"{func.__name__} took {end_time - start_time} seconds to execute.")
        return result
    return wrapper

@timing_decorator
def slow_function():
    # Simulating a time-consuming task
    time.sleep(2)
    print("Function executed!")

slow_function()
```

Output:

```
Function executed!
slow_function took 2.0054919719696045 seconds to execute.
```

## 고급 데코레이터 패턴
고급 데코레이터 패턴을 살펴보고 다재다능한 기능에 대한 이해의 폭을 넓히도록 하자.

### 인수가 있는 데코레이터

```python
def repeat(n):
    def decorator(func):
        def wrapper(*args, **kwargs):
            for _ in range(n):
                result = func(*args, **kwargs)
            return result
        return wrapper
    return decorator

@repeat(3)
def greet(name):
    print(f"Hello, {name}!")

greet("Alice")
```

Output:

```
Hello, Alice!
Hello, Alice!
Hello, Alice!
```

### 클래스 기반 데코레이터

```python
class LogDecorator:
    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        print(f"Calling {self.func.__name__} with arguments {args} {kwargs}")
        result = self.func(*args, **kwargs)
        print(f"{self.func.__name__} returned {result}")
        return result

@LogDecorator
def multiply(a, b):
    return a * b

result = multiply(4, 7)
```

Output:

```
Calling multiply with arguments (4, 7) {}
multiply returned 28
```

### 래퍼로서 데코레이터

```python
def exception_handler(func):
    def wrapper(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
        except Exception as e:
            print(f"Exception caught: {e}")
            result = None
        return result
    return wrapper

@exception_handler
def divide(a, b):
    return a / b

result = divide(5, 0)
```

Output:

```
Exception caught: division by zero
```

## 실제 적용 사례: 타이밍 데코레이터 재검토
함수의 실행 시간을 측정하고 기록하는 향상된 타이밍 데코레이터를 만들어 지금까지 배운 지식을 적용해 보겠다.

```python
import time

def timing_decorator(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"{func.__name__} took {end_time - start_time:.4f} seconds to execute.")
        return result
    return wrapper

@timing_decorator
def complex_computation():
    # Simulating a complex computation
    for _ in range(10**6):
        _ = _ * 2

complex_computation()
```

Output:

```
complex_computation took 0.0242 seconds to execute.
```

## 마치며
축하합니다! 이제 Python 데코레이터를 이해하기 위한 포괄적인 여정을 시작하였다. 기본 구조부터 고급 패턴과 실제 적용까지, 이 가이드는 Python 프로젝트에서 데코레이터를 효과적으로 활용할 수 있는 지식을 제공한다.
