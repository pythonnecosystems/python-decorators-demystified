# Python 데코레이터 이해하기: 고급 기법으로 코드의 완성도 높이기 <sup>[1](#footnote_1)</sup>


<a name="footnote_1">1</a>: [Python Decorators Demystified: Elevate Your Code with Advanced Techniques](https://medium.com/@karimmirzaguliyev/python-decorators-demystified-elevate-your-code-with-advanced-techniques-66efcf2d27bf)를 편역한 것이다.
